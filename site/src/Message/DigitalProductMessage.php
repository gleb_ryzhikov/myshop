<?php

namespace App\Message;

use App\Entity\DigitalProduct;
use App\Entity\User;

class DigitalProductMessage
{
    private DigitalProduct $product;
    private User $user;

    public function __construct(DigitalProduct $product, User $user)
    {
        $this->product = $product;
        $this->user = $user;
    }

    public function getProduct(): DigitalProduct
    {
        return $this->product;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}