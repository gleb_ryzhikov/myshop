<?php

namespace App\MessageHandler;

use App\Message\DigitalProductMessage;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class DigitalProductHandler
{
    public function __construct(
        private LoggerInterface $logger,
        private MailerInterface $mailer,
    )
    {
    }

    public function __invoke(DigitalProductMessage $userWithProd)
    {
        $prod = $userWithProd->getProduct();
        $user = $userWithProd->getUser();

        $uniqInt = random_int(200000, 300000);
        $hash = 0;
        $iterator = 1;

        while ($uniqInt > $hash) {
            $hash += gmp_prob_prime($iterator);
            $iterator ++;
        }

        // do some work
        $this->logger->info('it\'s work');

        $email = (new TemplatedEmail())
            ->from('gleb.r@zimalab.com')
            ->to($user->getEmail())
            ->subject('Congratulations!')
            ->htmlTemplate('mail/demo_digital.html.twig')
            ->context([
                'content' => $prod->getDescription(),
                'hash' => $hash,
            ])
        ;

        $this->mailer->send($email);

        printf('it\'s work');
    }
}