<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\Validator\ConstraintViolationList;
use Throwable;

class QuantityValidationException extends Exception
{
    public function __construct(ConstraintViolationList $constraintViolationList, int $code = 0, ?Throwable $previous = null)
    {
        $message = [];

        foreach ($constraintViolationList as $constraintViolation) {
            $message[] =  $constraintViolation->getMessage();
        }

        parent::__construct(implode("\n", $message), $code, $previous);
    }
}