<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ConstraintsQuantityProd extends Constraint
{
    public string $message = 'The quantity "{{ string }}" must less then existing quantity.';

    public function getMessage(): string
    {
        return $this->message;
    }
}