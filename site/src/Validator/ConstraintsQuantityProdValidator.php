<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConstraintsQuantityProdValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint)
    {
        $object = $this->context->getObject();

        if ($value > $object->getProduct()?->getCount()) {
            $this->context->buildViolation($constraint->getMessage())
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}