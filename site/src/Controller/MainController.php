<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    public function index(Request $request): Response
    {
        return $this->render('write.html.twig', [
            'host' => $request->getHost(),
        ]);
    }
}