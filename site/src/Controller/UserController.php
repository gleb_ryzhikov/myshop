<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\ShoppingCartModel;
use App\Service\UserModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function __construct(
        private UserModel         $userService,
        private ShoppingCartModel $cartService,
    )
    {
    }

    public function registration(Request $request): Response
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user, ['roles' => $this->getUser()?->getRoles()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->saveUserWithHashedPass($user);
            $this->cartService->createCartForUser($user);

            //TODO notice -> saved
            $this->addFlash('notice', 'Пользователь сохранен');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('user/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
