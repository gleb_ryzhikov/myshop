<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Exceptions\QuantityValidationException;
use App\Repository\CartProductRepository;
use App\Repository\ShoppingCartRepository;
use App\Service\ShoppingCartModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ShoppingCartController extends Controller
{
    public function __construct(
//        private ShoppingCartRepository $shoppingCartRepository,
//        private CartProductRepository $cartProductRepository,
        private ShoppingCartModel $shoppingCartService,
    )
    {
    }

    public function index(): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('Unreachable statement');
        }
        $cart = $user->getShoppingCart();

        $s = $cart->getCartProducts()->toArray();

        return $this->render('shopping_cart/index.html.twig', [
            'items' => $s,
        ]);
    }

    public function addToUserCart(Request $request, Product $product): Response
    {
        $quantity = $request->get('quantity');

        try {
            $this->shoppingCartService->addToUserCart($this->getUser(), $product, $quantity);
        } catch (QuantityValidationException $exception) {

            $this->addFlash(
                'error_quantity',
                $exception->getMessage(),
            );

            return $this->redirectToRoute('product_show', ['id' => $product->getId()]);
        }

        return $this->redirectToRoute('cart', [], Response::HTTP_SEE_OTHER);
    }

    public function removeFromUserCart(Product $product): Response
    {
        $this->shoppingCartService->deleteFromUserCart($this->getUser(), $product);

        return $this->redirectToRoute('cart', [], Response::HTTP_SEE_OTHER);
    }
}
