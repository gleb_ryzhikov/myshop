<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractDashboardController
{
    public function index(): Response
    {
//        return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
         return $this->render('@EasyAdmin/page/content.html.twig');
    }

    public function configureCrud(): Crud
    {
        return parent::configureCrud();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('myshop.loc');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Главная', 'fa fa-home'),

            MenuItem::section('Магазины'),
            MenuItem::linkToCrud('Категории', 'fas fa-list', Category::class)
                ->setController(CategoryCrudController::class)
                ->setAction('index'),
            MenuItem::linkToCrud('Продукты', 'fas fa-list', Product::class)
                ->setController(ProductCrudController::class),


            MenuItem::section('Orders'),
            MenuItem::linkToCrud('Orders', 'fas fa-list', Order::class)
                ->setController(OrderCrudController::class)
                ->setAction('index'),

            MenuItem::section('Администрация'),
            MenuItem::linkToCrud('Пользователи', 'fas fa-list', User::class)
                ->setController(UserCrudController::class)
                ->setPermission('ROLE_ADMIN'),

            MenuItem::linkToLogout('Выход', 'fa fa-exit'),
        ];
    }
}
