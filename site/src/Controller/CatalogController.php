<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\AbstractProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CatalogController extends Controller
{
    public function __construct(
        private AbstractProductRepository $productRepository,
        private CategoryRepository        $categoryRepository,
        private PaginatorInterface        $paginator,
    )
    {
    }

    public function index(
        Request $request,
        int $category = null,
    ): Response {

        $title = $request->get('search_title');

        $productsQuery = $this->productRepository->getQueryForPaginationByCategory($category, $title);
        $categoryQuery = $this->categoryRepository->findCategoryByExistProduct();


        $pagination = $this->paginator->paginate(
            $productsQuery,
            $request->query->getInt('page', 1),
            21
        );

        return $this->render('catalog/catalog_main.html.twig', [
            'pagination' => $pagination,
            'categories' => $categoryQuery,
        ]);
    }
}