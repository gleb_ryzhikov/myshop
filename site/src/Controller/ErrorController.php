<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class ErrorController extends Controller
{
    /**
     * Логика для фильтрации несуществующих страниц
     */
    public function checkRoute(): Response
    {
        return $this->forward('App\Controller\ErrorController::notFound');
    }
}