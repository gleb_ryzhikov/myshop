<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Entity\FullOrder;
use App\Form\FullOrderType;
use App\Service\OrderModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function __construct(
        private OrderModel $orderService,
    )
    {
    }

    public function index(): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('Unreachable statement');
        }

        $orders = $user->getOrders();

        return $this->render('order/index.html.twig', [
            'items' => $orders,
        ]);
    }

    public function buildOrder(Request $request): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('Unreachable statement');
        }

        $fullOrder = new FullOrder();
        $cartProd = $user->getShoppingCart()->getCartProducts();

        foreach ($cartProd as $item) {
            $fullOrder->addOrderItem($item->getProduct(), $item->getQuantity());
        }

        $form = $this->createForm(FullOrderType::class, $fullOrder);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->orderService->createOrder($fullOrder, $user);
            $this->orderService->removeCartProd($cartProd);
        }

        return $this->render('order/build.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}