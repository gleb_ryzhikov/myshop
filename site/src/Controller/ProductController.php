<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ExtendedCategoryProductType;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\AbstractProductRepository;
use App\Service\FileModel;
use App\Service\ProductModel;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function __construct(
        private ProductModel              $prodService,
        private AbstractProductRepository $prodRepo,
        private CategoryRepository        $categoryRepo,
        private FileModel                 $fileService,
    )
    {
    }

    public function show(Product $product, string $error = ''): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
            'error_quantity' => $error,
        ]);
    }

    public function editCollection(Request $request): Response
    {
        $category = $this->categoryRepo->findBy([], null, 1);
        $prodCollection = new ArrayCollection($this->prodRepo->findBy(['category' => $category], null, 10));

        $form = $this->createForm(
            ExtendedCategoryProductType::class,
            [
                'category' => $category[0],
                'product' => $prodCollection,
            ],
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->prodService->saveProdCollection($category, $prodCollection);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('catalog/edit_collection.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function editImage(Request $request, ?Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);;
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('image')->getData();

            $fileName = $file ? $this->fileService->save($file) : null;

            $this->prodService->saveWithImage($product, $fileName);

            return $this->forward('App\Controller\ProductController::show', ['product' => $product]);
        }

        return $this->render('product/edit_image.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteImage(Product $product): Response
    {
        unlink($product->getPathImage());
        $product->setImage();
        $this->prodRepo->add($product);

        return $this->forward('App\Controller\ProductController::show', ['product' => $product]);
    }
}
