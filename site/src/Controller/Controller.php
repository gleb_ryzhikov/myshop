<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class Controller extends AbstractController
{
    /**
     * Рендер раздел в разработке
     */
    public function developing(): Response
    {
        return $this->render('main/error_in_develop.html.twig');
    }

    /**
     * 404
     */
    public function notFound(): Response
    {
        return $this->render('main/error_404.html.twig');
    }

//    protected function getUser(): User
//    {
//        return parent::getUser();
//    }
}