<?php

namespace App\Service;


use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileModel
{
    private int $minSize = 150;

    public function __construct(
        private CacheManager $cacheManager,
    )
    {
    }

    public function save(UploadedFile $file, $path = 'upload/'): ?string
    {
        $extension = $file->guessExtension();

        $fileName = $fileNameOriginal = uniqid('_prod_org_') . '.' . $extension;

        $file->move($path, $fileName);

        return $fileNameOriginal;
    }
}