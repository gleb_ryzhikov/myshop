<?php

namespace App\Service;

use App\Entity\AbstractProduct;
use App\Entity\CartProduct;
use App\Entity\ShoppingCart;
use App\Entity\User;
use App\Exceptions\QuantityValidationException;
use App\Repository\CartProductRepository;
use App\Repository\ShoppingCartRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Exception\LogicException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ShoppingCartModel
{
    public function __construct(
        private LoggerInterface $logger,
        private ShoppingCartRepository $cartRepository,
        private CartProductRepository $cartProductRepository,
        private ValidatorInterface $validator,
    ) {}

    public function createCartForUser(User $user): void
    {
        $this->logger->info('Start create cart for user', ['user' => $user->getId()]);
        $cart = new ShoppingCart();

        $cart->setUser($user);

        $this->cartRepository->add($cart);
        $this->logger->info('Create cart success', ['user' => $user->getEmail()]);
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws QuantityValidationException
     * @throws \Doctrine\ORM\ORMException
     */
    public function addToUserCart(User $user, AbstractProduct $product, int $quantity): void //?string
    {
        $this->logger->info("Add product to user cart", [
            'user_email' => $user?->getEmail(),
            'product' => $product
        ]);

        // TODO if $user = null generate cart
        $newItem = new CartProduct();

        $newItem->setCart($user?->getShoppingCart());
        $newItem->setProduct($product);
        $newItem->setQuantity($quantity);

        $violationList = $this->validator->validate($newItem);

        if ($violationList->count()) {
//            return $violationList->get(0)->getMessage();

            throw new QuantityValidationException($violationList);
        }

        $this->cartProductRepository->add($newItem);

//        return null;
    }

    public function deleteFromUserCart(User $user, AbstractProduct $product): void
    {
        $this->logger->info("Deleted product from user cart", [
            'user_email' => $user?->getEmail(),
            'product' => $product
        ]);

        $pr = $this->cartProductRepository->findCartProductForRemove($user?->getShoppingCart(), $product);

        $this->cartProductRepository->remove($pr);
    }
}
