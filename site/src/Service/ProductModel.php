<?php

namespace App\Service;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ProductModel
{
    public function __construct(
        private EntityManagerInterface $em,
        private LoggerInterface        $logger,
    )
    {
    }

    public function saveProdCollection(mixed $category, mixed $product): void
    {
        $this->logger->info('Save product collection',
            [
                'category' => $category,
                'product' => $product,
            ]
        );

        $this->em->persist($category[0]);

        foreach ($product as $item) {
            $this->em->persist($item);
        }

        $this->em->flush();
    }

    public function saveWithImage(Product $product, $file = null): void
    {
        $product->setImage($file);

        $this->em->persist($product);

        $this->em->flush();

        $this->logger->info('Save product with image', ['product' => $product, 'file' => $file]);
    }
}