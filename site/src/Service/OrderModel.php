<?php

namespace App\Service;

use App\Entity\DigitalProduct;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\User;
use App\Message\DigitalProductMessage;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderModel
{
    public function __construct(
        private EntityManagerInterface $em,
        private LoggerInterface        $logger,
        private MailerInterface        $mailer,
        private MessageBusInterface    $bus,
    )
    {
    }

    public function createOrder(mixed $data, User $user): void
    {
        $orderDb = new Order();

        $orderDb->setUser($user);
        $orderDb->setDate(new \DateTime());
        $orderDb->setStatus(Order::CREATE_TYPE);

        $this->em->persist($orderDb);

        $this->em->flush();


        foreach ($data->getOrderItems() as $order) {
            $prod = $order->getProduct();

//            if ($prod instanceof DigitalProduct) {
//                $this->bus->dispatch(new DigitalProductMessage($prod, $user));
//            }

            $orderEntity = new OrderItem();

            $orderEntity->setProduct($prod);
            $orderEntity->setQuantity($order->getQuantity());
            $orderEntity->setCost($order->getProduct()->getCost());
            $orderEntity->setOrder($orderDb);

            $this->em->persist($orderEntity);

            $this->sendNotifyForConfirmOrder($orderEntity);
        }

        $this->em->flush();
    }

    public function removeCartProd(Collection $cartProds): void
    {
        foreach ($cartProds as $item) {
            $this->em->remove($item);
        }

        $this->em->flush();
    }

    /**
     * send mail to administrator
     * @return void
     */
    private function sendNotifyForConfirmOrder(OrderItem $order): void
    {
        $this->logger->info('Please, confirm order', ['order' => $order]);
    }
}