<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserModel
{
    public function __construct(
        private EntityManagerInterface $em,
        private UserPasswordHasherInterface $hasher,
        private LoggerInterface $logger,
        private MailerInterface $mailer,
    ) {}

    public function saveUserWithHashedPass(User $user): void
    {
        $this->logger->info('Hash password for user', ['user_email' => $user->getEmail()]);

        $pass = $this->hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($pass);

        $this->em->persist($user);

        $this->em->flush();
        $this->logger->info('User save with ID ', ['user_id' => $user->getId()]);

        $email = (new TemplatedEmail())
            ->from('gleb.r@zimalab.com')
            ->to($user->getEmail())
            ->subject('Congratulations!')
            ->htmlTemplate('mail/register.html.twig')
            ->context([
                'mailAddress' => $user->getEmail(),
            ])
        ;

        $this->mailer->send($email);
    }
}
