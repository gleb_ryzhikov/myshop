<?php

namespace App\EventListener;

use App\Entity\DigitalProduct;
use App\Entity\Order;
use App\Entity\Product;
use App\Message\DigitalProductMessage;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Messenger\MessageBusInterface;

class ChangeStatusOrderListener
{
    public function __construct(
        private MessageBusInterface    $bus,
        private EntityManagerInterface $em,
    )
    {
    }

    public function preUpdate(Order $order, PreUpdateEventArgs $args): void
    {
        if ($args->hasChangedField('status') && $order->getStatus() === Order::CONFIRM_TYPE) {
            foreach ($order->getItems() as $item) {
                $prod = $item->getProduct();

                if ($prod instanceof DigitalProduct) {
                    $this->bus->dispatch(new DigitalProductMessage($prod, $order->getUser()));
                }

                if ($prod instanceof Product) {
                    $prod->setCount($prod->getCount() - $item->getQuantity());
                    $this->em->persist($prod);
                }
            }
        } else {
            printf('BBBBBBBBBBBBBBBBBBBBBBBBBBB');
        }

    }

}