<?php

namespace App\EventListener;

use App\Entity\OrderItem;
use Doctrine\ORM\Event\PreFlushEventArgs;

class CreateOrderListener
{
    public function __construct()
    {
    }

    public function preFlush(OrderItem $item, PreFlushEventArgs $args): void
    {
    }
}