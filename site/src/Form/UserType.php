<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', TextType::class)
            ->add('password', TextType::class)
            ->add('passwordReenter', TextType::class)
        ;

        if (!$options['roles']) {
            $builder->add('roles', HiddenType::class, [
               'data' => [null],
            ]);
        } else {
            $builder
                ->add('roles', ChoiceType::class, [
                    'choices' => [
                        User::ROLES[User::MANAGER_TYPE] => User::MANAGER_TYPE,
                        User::ROLES[User::ADMIN_TYPE] => User::ADMIN_TYPE,
                        User::ROLES[User::USER_TYPE] => null,
                    ],
                    'required' => false,
                ]);
        }

        $builder->add('save', SubmitType::class);

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($rolesArray) {
                    return count($rolesArray)? $rolesArray[0]: null;
                },
                function ($rolesString) {
                    return [$rolesString];
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'roles' => [],
        ]);
    }
}
