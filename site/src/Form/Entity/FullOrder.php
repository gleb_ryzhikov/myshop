<?php

namespace App\Form\Entity;

use App\Entity\AbstractProduct;
use App\Entity\OrderItem;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class FullOrder
{
    private ?Collection $orderItems;

    public function __construct()
    {
         $this->orderItems = new ArrayCollection();
    }

    /**
     * @param Collection $orderItems
     */
    public function setOrderItems(Collection $orderItems): void
    {
        $this->orderItems = $orderItems;
    }

    /**
     * @return Collection
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(AbstractProduct $product, int $quantity): self
    {
        $obj = new OrderItem();
        $obj->setProduct($product);
        $obj->setQuantity($quantity);

        $this->orderItems->add($obj);

        return $this;
    }
}