<?php

namespace App\Repository;

use App\Entity\AbstractProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AbstractProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractProduct[]    findAll()
 * @method AbstractProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractProduct::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(AbstractProduct $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(AbstractProduct $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @return Query
     */
    public function getQueryForPagination(): Query
    {
        return $this->createQueryBuilder('p')->getQuery();
    }

    /**
     * @param int|null $category - id category
     * @return Query
     */
    public function getQueryForPaginationByCategory(?int $category, ?string $search): Query
    {
        $qb = $this->createQueryBuilder('p')
//            ->where('p NOT INSTANCE OF App\Entity\DigitalAbstractProduct AND p NOT INSTANCE OF App\Entity\Service')
        ;

        if ($category) {
            $qb
                ->andWhere('p.category = :category')
                ->setParameter('category', $category)
            ;
        }

        if ($search) {
            $qb
                ->andWhere('p.title LIKE :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

//        dd($qb->getQuery()->getSQL());

        return $qb->getQuery();
    }
}
