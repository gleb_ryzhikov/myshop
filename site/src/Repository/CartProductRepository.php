<?php

namespace App\Repository;

use App\Entity\CartProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CartProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartProduct[]    findAll()
 * @method CartProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartProduct::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(CartProduct $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(CartProduct $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return CartProduct[] Returns an array of CartProduct objects
    //  */
    public function findProductByFields($cart = null, $product = null)
    {
        $qb = $this->createQueryBuilder('cp')
            ->innerJoin('cp.product', 'p')
        ;

        if ($cart) {
            $qb->andWhere('cp.cart = :cart')
                ->setParameter('cart', $cart->getId())
            ;
        }

        if ($product) {
            $qb->andWhere('cp.product = :product')
                ->setParameter('product', $product->getId())
            ;
        }

        return $qb->getQuery()
            ->getResult()
            ;
    }


    public function findCartProductForRemove($cart = null, $product = null)
    {
        return $this->findOneBy([
            'product' => $product,
            'cart' => $cart,
        ]);
    }

    /*
    public function findOneBySomeField($value): ?CartProduct
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
