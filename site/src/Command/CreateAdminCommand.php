<?php

namespace App\Command;

use App\Entity\User;
use App\Service\UserModel;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-admin',
    description: 'Create administrator for myshop.loc',
)]
class CreateAdminCommand extends Command
{
    public function __construct(private UserModel $service, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'Enter administrator email')
            ->addArgument('password1', InputArgument::REQUIRED, 'Enter password')
            ->addArgument('password2', InputArgument::REQUIRED, 'Re-enter password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $email = $input->getArgument('email');

        if (!$email || !preg_match('/\w+@\w+\.\w+/', $email)) {
            $io->warning(sprintf('You passed an argument: %s', $email));
            return Command::FAILURE;
        }

        $password = $input->getArgument('password1');
        $passwordReentred = $input->getArgument('password2');

        if ($password && $passwordReentred) {
            if ($password === $passwordReentred) {
                $user = new User();
                $user->setEmail($email);
                $user->setPassword($password);
                $user->setRoles(['ROLE_ADMIN']);

                $this->service->saveUserWithHashedPass($user);

                $io->success('Administrator with ' . $email . ' saved!');
                return Command::SUCCESS;
            }

            $io->warning(sprintf('Passwords don\'t equal'));
            return Command::FAILURE;
        }

        $io->warning(sprintf('Please check input data (email, password, repeat password)'));

        return Command::FAILURE;
    }
}
