<?php

namespace App\Entity;

use App\Repository\AbstractProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AbstractProductRepository::class)]
class Product extends AbstractProduct
{
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $count = null;

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }
}
