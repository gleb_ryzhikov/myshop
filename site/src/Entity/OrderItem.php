<?php

namespace App\Entity;

use App\Repository\OrderItemRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderItemRepository::class)]
#[ORM\Table(name: '`order_item`')]
class OrderItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: AbstractProduct::class, inversedBy: 'orders')]
    private ?AbstractProduct $product;

    #[ORM\Column(type: 'integer', nullable: true)]
    private int $quantity;

    #[ORM\Column(type: 'float')]
    private float $cost;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'item')]
    #[ORM\JoinColumn(nullable: false)]
    private $order;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): AbstractProduct
    {
        return $this->product;
    }

    public function setProduct(AbstractProduct $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }
}
