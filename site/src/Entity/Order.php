<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    public const CREATE_TYPE = 0;
    public const CONFIRM_TYPE = 1;
    public const FORMED_TYPE = 2;
    public const SEND_TYPE = 3;
    public const ERROR_TYPE = 4;
    public const RECEIVED_TYPE = 5;

    public const STATUS = [
        self::CREATE_TYPE => 'Создан',
        self::CONFIRM_TYPE => 'Подтвержден',
        self::FORMED_TYPE => 'Сформирован',
        self::SEND_TYPE => 'Отправлен',
        self::ERROR_TYPE => 'Произошла ошибка',
        self::RECEIVED_TYPE => 'Получен',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'ordersMain')]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\OneToMany(mappedBy: 'order', targetEntity: OrderItem::class, orphanRemoval: true)]
    private Collection $items;

    #[ORM\Column(type: 'datetimetz')]
    private ?\DateTimeInterface $date;

    #[ORM\Column(type: 'integer', nullable: true)]
    private int $status;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getTitleProducts(): string
    {
        $tempArr = [];

        foreach ($this->getItems() as $prod) {
            $tempArr[] = $prod->getProduct()->getTitle();
        }

        return implode(', ', $tempArr);
    }

    public function addProduct(OrderItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setOrder($this);
        }

        return $this;
    }

    public function removeProduct(OrderItem $item): self
    {
        if ($this->items->removeElement($item)) {
            if ($item->getOrder() === $this) {
                $item->setOrder(null);
            }
        }

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getTitleStatus(int $stat): string
    {
        return self::STATUS[$stat];
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
