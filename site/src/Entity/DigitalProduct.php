<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class DigitalProduct extends AbstractProduct
{
    #[ORM\Column(type: 'integer', nullable: false)]
    private ?int $numberOfInstance = null;

    public function getNumberOfInstance(): ?string
    {
        return $this->numberOfInstance;
    }

    public function setNumberOfInstance(?int $numberOfInstance): self
    {
        $this->numberOfInstance = $numberOfInstance;

        return $this;
    }
}
