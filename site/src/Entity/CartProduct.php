<?php

namespace App\Entity;

use App\Repository\CartProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CartProductRepository::class)]
class CartProduct
{
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: ShoppingCart::class, inversedBy: 'cartProducts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ShoppingCart $cart;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: AbstractProduct::class, inversedBy: 'cartProducts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?AbstractProduct $product;

    #[ORM\Column(type: 'integer')]
    private int $quantity;

    public function getCart(): ?ShoppingCart
    {
        return $this->cart;
    }

    public function setCart(?ShoppingCart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getProduct(): ?AbstractProduct
    {
        return $this->product;
    }

    public function setProduct(?AbstractProduct $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
