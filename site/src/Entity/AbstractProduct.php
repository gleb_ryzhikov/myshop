<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

#[ORM\Table('product')]
#[ORM\Entity]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'entity', type: 'string')]
#[ORM\DiscriminatorMap(['product' => Product::class, 'service' => Service::class, 'digital' => DigitalProduct::class])]
abstract class AbstractProduct
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 50)]
    protected ?string $title;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $description = null;

    #[ORM\Column(type: 'float', nullable: true)]
    protected ?float $cost = null;

    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $image = null;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Category $category;

    #[ORM\ManyToMany(targetEntity: ShoppingCart::class, mappedBy: 'product')]
    protected ?Collection $shoppingCarts;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: CartProduct::class)]
    protected ?Collection $cartProducts;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: OrderItem::class)]
    protected ?Collection $orders;

    #[Pure] public function __construct()
    {
        $this->shoppingCarts = new ArrayCollection();
        $this->cartProducts = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(?float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getPathImage(): ?string
    {
        return 'upload/' . $this->image;
    }

    public function setImage(?string $file = null): self
    {
        $this->image = $file;

        return $this;
    }

    public function getShoppingCarts(): Collection
    {
        return $this->shoppingCarts;
    }

    public function addShoppingCart(ShoppingCart $shoppingCart): self
    {
        if (!$this->shoppingCarts->contains($shoppingCart)) {
            $this->shoppingCarts->add($shoppingCart);
            $shoppingCart->addProduct($this);
        }

        return $this;
    }

    public function removeShoppingCart(ShoppingCart $shoppingCart): self
    {
        if ($this->shoppingCarts->removeElement($shoppingCart)) {
            $shoppingCart->removeProductElement($this);
        }

        return $this;
    }

    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setProduct($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->removeElement($cartProduct)) {
            if ($cartProduct->getProduct() === $this) {
                $cartProduct->setProduct(null);
            }
        }

        return $this;
    }

    public function getOrders(): ?Collection
    {
        return $this->orders;
    }

    public function setOrders(?OrderItem $orders): self
    {
        $this->orders->add($orders);

        return $this;
    }
}