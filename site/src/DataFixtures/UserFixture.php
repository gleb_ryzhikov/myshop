<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixture extends Fixture
{

    public function __construct(private UserPasswordHasherInterface $hasher) {}

    /**
     * @param EntityManagerInterface $manager
     * @return void
     * @throws \Doctrine\ORM\ORMException
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++ ) {
            $user = new User();
            $user->setEmail($faker->email);
            $pass = $faker->password;
            $hashPass = $this->hasher->hashPassword($user, $pass);
            $user->setPassword($hashPass);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
