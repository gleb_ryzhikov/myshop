<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\DigitalProduct;
use App\Entity\Product;
use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProdFixture extends Fixture
{
    private array $categories = [];

    private $faker;

    /**
     * @param EntityManagerInterface $manager
     * @return void
     * @throws \Doctrine\ORM\ORMException
     */
    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create();

        for ($i = 0; $i < 100; $i++ ) {
            $categories[] = $category = new Category();
            $category->setName($this->faker->word);

            $manager->persist($category);
        }

        $manager->flush();
        $manager->clear();
        $this->categories = array_map(fn (Category $c) => $c->getId(), $categories);

        for ($i = 0; $i < 5000; $i++) {
            // bullshit
            if ($i < 3000) {
                $item = new Product();
            } elseif ($i < 4000) {
                $item = new Service();
            } else {
                $item = new DigitalProduct();
            }

            $this->fillingItem($item, $i, $manager);

            $manager->persist($item);
        }

        $manager->flush();
    }

    private function fillingItem(&$object, $number, $manager): void
    {
        $categoryId = $this->categories[array_rand($this->categories)];

        /** @var Category $category */
        $category = $manager->getReference(Category::class, $categoryId);
        $object->setCategory($category);
        $object->setCost(mt_rand(100, 1000000) / 1000);

        if ($object instanceof Service) {
            $object->setExecutionTime(mt_rand(0, 200));
        } elseif ($object instanceof DigitalProduct) {
            $object->setNumberOfInstance($number);
        } elseif ($object instanceof Product) {
            $object->setCount(mt_rand(0, 200));
        }

        $object->setDescription($this->faker->text);
        $object->setTitle($this->faker->name);
    }

}
